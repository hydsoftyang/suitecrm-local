<?php /* Smarty version 2.6.29, created on 2017-11-07 15:10:53
         compiled from cache/include/InlineEditing/Docke_Docker_modEditViewdocker_avatar.tpl */ ?>



<script type="text/javascript">
    <?php echo '
        $( document ).ready(function() {
        $( "form#EditView" )
        .attr( "enctype", "multipart/form-data" )
        .attr( "encoding", "multipart/form-data" );
    });
'; ?>

</script>
<script type="text/javascript" src='include/SugarFields/Fields/Image/SugarFieldFile.js?v=ZYFViqrkrLoqSHXrmOkZAg'></script>

<?php if (! empty ( $this->_tpl_vars['fields']['docker_avatar']['value'] )): ?>
    <?php $this->assign('showRemove', true); ?>
<?php else: ?>
    <?php $this->assign('showRemove', false); ?>
<?php endif; ?>

<?php $this->assign('noChange', false); ?>

<input type="hidden" name="deleteAttachment" value="0">
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['docker_avatar']['value']; ?>
">
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_record_id" id="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_record_id" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<span id="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_old" style="display:<?php if (! $this->_tpl_vars['showRemove']): ?>none;<?php endif; ?>">
  <a href="index.php?entryPoint=download&id=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
_<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
&type=<?php echo $this->_tpl_vars['module']; ?>
&time=<?php echo $this->_tpl_vars['fields']['date_modified']['value']; ?>
" class="tabDetailViewDFLink"><?php echo $this->_tpl_vars['fields']['docker_avatar']['value']; ?>
</a>

        <?php if (! $this->_tpl_vars['noChange']): ?>
        <input type='button' class='button' id='remove_button' value='<?php echo $this->_tpl_vars['APP']['LBL_REMOVE']; ?>
' onclick='SUGAR.field.file.deleteAttachment("<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
","",this);'>
    <?php endif; ?>
</span>
<?php if (! $this->_tpl_vars['noChange']): ?>
<span id="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_new" style="display:<?php if ($this->_tpl_vars['showRemove']): ?>none;<?php endif; ?>">
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_escaped">
<input id="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_file" name="<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_file"
       type="file" title='' size="30"
                       maxlength='255'
                >

    <?php else: ?>



<?php endif; ?>

<script type="text/javascript">
$( "#<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_file<?php echo ' " ).change(function() {
        $("#'; ?>
<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
<?php echo '").val($("#'; ?>
<?php echo $this->_tpl_vars['fields']['docker_avatar']['name']; ?>
_file<?php echo '").val());
});'; ?>

        </script>


</span>