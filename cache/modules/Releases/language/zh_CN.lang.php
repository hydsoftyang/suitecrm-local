<?php
// created: 2017-11-07 14:05:53
$mod_strings = array (
  'LBL_MODULE_NAME' => '版本',
  'LBL_MODULE_TITLE' => '版本',
  'LBL_SEARCH_FORM_TITLE' => '搜索',
  'LBL_LIST_FORM_TITLE' => '版本列表',
  'LBL_NEW_FORM_TITLE' => '新建',
  'LBL_RELEASE' => '版本',
  'LBL_LIST_NAME' => '名称',
  'LBL_NAME' => '名称',
  'LBL_LIST_LIST_ORDER' => '顺序',
  'LBL_LIST_ORDER' => '顺序',
  'LBL_LIST_STATUS' => '状态',
  'LBL_STATUS' => '状态',
  'LNK_NEW_RELEASE' => '创建版本',
  'NTC_DELETE_CONFIRMATION' => '您确定要删除此项记录？',
  'ERR_DELETE_RECORD' => '您必须指定记录编号才能删除。',
  'NTC_STATUS' => '设置状态为 "停用"，将其从下拉列表中移除。',
  'NTC_LIST_ORDER' => '设置在下拉列表中的显示顺序',
  'release_status_dom' => 
  array (
    'Active' => '启用',
    'Inactive' => '停用',
  ),
  'LBL_EDITLAYOUT' => '编辑布局',
);