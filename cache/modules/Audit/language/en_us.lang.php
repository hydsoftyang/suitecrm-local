<?php
// created: 2017-11-07 15:26:13
$mod_strings = array (
  'LBL_FIELD_NAME' => 'Field',
  'LBL_OLD_NAME' => 'Old Value',
  'LBL_NEW_VALUE' => 'New Value',
  'LBL_CREATED_BY' => 'Changed By',
  'LBL_LIST_DATE' => 'Change Date',
  'LBL_AUDITED_FIELDS' => 'Fields audited in this module: ',
  'LBL_CHANGE_LOG' => 'Change Log',
);