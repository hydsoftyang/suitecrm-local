<?php
// created: 2017-11-07 14:28:20
$dictionary["docke_docker_mod_accounts"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'docke_docker_mod_accounts' => 
    array (
      'lhs_module' => 'Docke_Docker_mod',
      'lhs_table' => 'docke_docker_mod',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'docke_docker_mod_accounts_c',
      'join_key_lhs' => 'docke_docker_mod_accountsdocke_docker_mod_ida',
      'join_key_rhs' => 'docke_docker_mod_accountsaccounts_idb',
    ),
  ),
  'table' => 'docke_docker_mod_accounts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'docke_docker_mod_accountsdocke_docker_mod_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'docke_docker_mod_accountsaccounts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'docke_docker_mod_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'docke_docker_mod_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'docke_docker_mod_accountsdocke_docker_mod_ida',
        1 => 'docke_docker_mod_accountsaccounts_idb',
      ),
    ),
  ),
);