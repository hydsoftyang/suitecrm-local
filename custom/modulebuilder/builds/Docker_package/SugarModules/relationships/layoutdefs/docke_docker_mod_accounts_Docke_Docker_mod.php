<?php
 // created: 2017-11-07 14:28:20
$layout_defs["Docke_Docker_mod"]["subpanel_setup"]['docke_docker_mod_accounts'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCKE_DOCKER_MOD_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'docke_docker_mod_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
