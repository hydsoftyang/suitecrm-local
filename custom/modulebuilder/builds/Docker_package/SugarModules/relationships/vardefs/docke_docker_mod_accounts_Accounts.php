<?php
// created: 2017-11-07 14:28:20
$dictionary["Account"]["fields"]["docke_docker_mod_accounts"] = array (
  'name' => 'docke_docker_mod_accounts',
  'type' => 'link',
  'relationship' => 'docke_docker_mod_accounts',
  'source' => 'non-db',
  'module' => 'Docke_Docker_mod',
  'bean_name' => false,
  'vname' => 'LBL_DOCKE_DOCKER_MOD_ACCOUNTS_FROM_DOCKE_DOCKER_MOD_TITLE',
);
