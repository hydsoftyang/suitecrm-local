<?php
// created: 2017-11-07 14:28:20
$dictionary["Docke_Docker_mod"]["fields"]["docke_docker_mod_accounts"] = array (
  'name' => 'docke_docker_mod_accounts',
  'type' => 'link',
  'relationship' => 'docke_docker_mod_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_DOCKE_DOCKER_MOD_ACCOUNTS_FROM_ACCOUNTS_TITLE',
);
